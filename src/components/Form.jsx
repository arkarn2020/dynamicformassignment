import React from "react";
import { useForm } from "react-hook-form";

const Form = ({ template: fields, formDataSubmit, isMutating }) => {
  //   console.log(fields);
  const { register, handleSubmit } = useForm();

  const renderFields = (fields) => {
    return fields.map((field, i) => {
      let { type, fieldName: name, value, options, validationProps } = field;

      switch (type) {
        case "text":
          return (
            <div key={i}>
              <label htmlFor={name}>{name.toUpperCase()}</label>
              <input
                type="text"
                id={name}
                defaultValue={value}
                {...register(name, validationProps)}
              />
            </div>
          );
        case "email":
          return (
            <div key={i}>
              <label htmlFor={name}>{name.toUpperCase()}</label>
              <input
                type="email"
                id={name}
                defaultValue={value}
                {...register(name, validationProps)}
              />
            </div>
          );
        case "number":
          return (
            <div key={i}>
              <label htmlFor={name}>{name.toUpperCase()}</label>
              <input
                type="number"
                id={name}
                defaultValue={value}
                {...register(name, validationProps)}
              />
            </div>
          );
        case "select":
          return (
            <div key={i}>
              <label htmlFor={name}>{name.toUpperCase()}</label>
              <select
                name={name}
                id={name}
                {...register(name, validationProps)}
              >
                {options.map((gender, i) => (
                  <option key={i}>{gender}</option>
                ))}
              </select>
            </div>
          );
        case "multiline":
          return (
            <div key={i}>
              <label htmlFor={name}>{name.toUpperCase()}</label>
              <textarea
                id={name}
                defaultValue={value}
                rows={5}
                cols={45}
                {...register(name, validationProps)}
              />
            </div>
          );
        default:
          return <div key={i}>invalid field</div>;
      }
    });
  };

  return (
    <form onSubmit={handleSubmit(formDataSubmit)}>
      {renderFields(fields)}
      <br />
      <button type="submit">{isMutating ? "submitting..." : "submit"}</button>
    </form>
  );
};

export default Form;

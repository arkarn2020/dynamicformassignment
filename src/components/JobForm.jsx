import React, { useState } from "react";
import Form from "./Form";
import axios from "axios";
import { useQuery, useMutation } from "react-query";
import RenderResponse from "./RenderResponse";

const fetchFormData = async () => {
  return await axios
    .get("https://ulventech-react-exam.netlify.app/api/form")
    .then((res) => res.data)
    .catch((error) => console.log(error.message));
};

const JobForm = () => {
  const { data, isLoading } = useQuery("serverformdata", () => fetchFormData());

  const [response, setResponse] = useState();

  const submitFormData = async (submittedData) => {
    return await axios
      .post("https://ulventech-react-exam.netlify.app/api/form", submittedData)
      .then((res) => {
        setResponse(res.data);
        // console.log(response);
      })
      .catch((error) => console.log(error.message));
  };

  const { mutateAsync, isLoading: isMutating } = useMutation(submitFormData);

  const formDataSubmit = async (values) => {
    await mutateAsync(values);
  };

  if (isLoading) return <>loading...</>;

  return (
    <>
      <Form
        template={data?.data.map((d) => d)}
        formDataSubmit={formDataSubmit}
        isMutating={isMutating}
      />
      <br />
      <div>{response !== undefined && <RenderResponse obj={response} />}</div>
    </>
  );
};

export default JobForm;

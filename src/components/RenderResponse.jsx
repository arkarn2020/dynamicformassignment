import React from "react";

const RenderResponse = ({ obj }) => {
  return (
    <>
      <h3>Response</h3>
      <div>&#123;</div>
      <div>
        {Object.entries(obj).map((e, i, arr) =>
          arr.length - 1 === i ? (
            <div key={i}>
              <div>"data": &#123;</div>
              {Object.entries(obj.data).map((el, il, arrl) =>
                arrl.length - 1 === il ? (
                  <p key={il}>{`"${el[0]}": "${el[1]}"`}</p>
                ) : (
                  <p key={il}>{`"${el[0]}": "${el[1]}",`}</p>
                )
              )}
              <div>&#125;</div>
            </div>
          ) : (
            <div key={i}>{`"${e[0]}": ${e[1]},`}</div>
          )
        )}
      </div>
      <div>&#125;</div>
    </>
  );
};

export default RenderResponse;
